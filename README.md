# ** Domo Pi** #

Cliente Android para gestionar los gpio, sensores y conversores de una Raspberry Pi configurada correctamente con WebIOPi: https://code.google.com/p/webiopi/

Supone que están activos los canales: 7, 8, 9, 11 y 25 de la raspberry Pi (como OUT) y el canal 10 será tratado como IN:apagado y OUT:encendido.

- Actualización instantánea del cambio de estado de los gpio (OUT).

- Refresco automático del estado de los gpio (OUT).

- Auto encendido y apagado de los gpio indicados (se necesita estar activo con el dispositivo y conectado a la red para no perder el evento de esa alarma).



# ** Desde** #

26 de abril de 2015



# ** Autores** #

Borja CR

Cristian TG

Daniel RS