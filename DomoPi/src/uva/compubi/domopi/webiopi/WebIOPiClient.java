package uva.compubi.domopi.webiopi;

import java.util.Hashtable;
import java.util.Set;

import android.annotation.SuppressLint;

import com.trouch.webiopi.client.PiClient;
import com.trouch.webiopi.client.PiHttpClient;
import com.trouch.webiopi.client.devices.digital.GPIO;
import com.trouch.webiopi.client.devices.digital.NativeGPIO;

/**
 * Cliente que utiliza la interfaz de WebIOPi.
 * 
 * @author Cristian TG
 * @author Daniel RS
 * @author Borja CR
 * @since 0.1-ALPHA
 * @version 0.2-BETA
 */
public final class WebIOPiClient {

	/**
	 * Cliente de GPIO.
	 */
	private NativeGPIO gpio;

	/**
	 * No se permite creaci�n sin par�metros.
	 */
	@SuppressWarnings("unused")
	private WebIOPiClient() {

	}

	/**
	 * Constructor por defecto. Establece la ruta de conexi�n (pero no se
	 * conecta al servidor para verificar).
	 * 
	 * @param host
	 *            URL de conexi�n.
	 * @param port
	 *            Puerto de la URL de conexi�n.
	 * @param user
	 *            Usuario para la conexi�n.
	 * @param password
	 *            Contrase�a para el usuario de la conexi�n.
	 */
	public WebIOPiClient(final String host, final int port, final String user,
			final String password) {
		final PiClient client = new PiHttpClient(host, port);
		client.setCredentials(user, password);
		this.gpio = new NativeGPIO(client);
	}

	final static byte SPECIAL_CHANNEL = 10;

	/**
	 * Establece el estado de los canales gpio indicados. Si es el canal
	 * {@link WebIOPiClient#SPECIAL_CHANNEL} establecer� la funci�n IN:OFF y
	 * OUT:ON
	 * 
	 * @param channels
	 *            Canales y sus estados a establecer.
	 * @throws Exception
	 *             Si hubo alg�n problema de conexi�n.
	 */
	public void setGPIOStatus(final Hashtable<Integer, Boolean> channels)
			throws Exception {
		final Set<Integer> keys = channels.keySet();
		for (final Integer key : keys) {
			if (key != SPECIAL_CHANNEL) {
				this.gpio.digitalWrite(key, channels.get(key));
			} else {
				this.gpio.setFunction(key, channels.get(key) ? GPIO.OUT
						: GPIO.IN);
			}
		}
	}

	/**
	 * Obtiene el estado de los canales gpio indicados.
	 * 
	 * @param channels
	 *            Canales a obtener su estado. El canal
	 *            {@link WebIOPiClient#SPECIAL_CHANNEL} se tomar� como IN:OFF y
	 *            OUT:ON
	 * @return Tabla hash de pares: canal-estado
	 * @throws Exception
	 *             Si hubo alg�n problema de conexi�n.
	 */
	@SuppressLint("DefaultLocale")
	public Hashtable<Integer, Boolean> getGPIOStatus(final int[] channels)
			throws Exception {
		final Hashtable<Integer, Boolean> channelStatus = new Hashtable<Integer, Boolean>();

		int CHANNELS_LENGTH_1 = channels.length - 1;
		int currentChannel;
		String function;
		for (int i = CHANNELS_LENGTH_1; i >= 0; i--) {
			currentChannel = channels[i];
			if (currentChannel != SPECIAL_CHANNEL) {
				channelStatus.put(currentChannel,
						this.gpio.digitalRead(currentChannel));
			} else {
				function = this.gpio.getFunction(currentChannel).replaceAll(
						"\n", "");
				channelStatus.put(currentChannel,
						function.equalsIgnoreCase(GPIO.OUT));
			}

		}
		return channelStatus;
	}
}