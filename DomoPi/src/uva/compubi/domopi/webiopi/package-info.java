/**
 * Contiene todos los comunicantes con la interfaz de WebIOPi.
 *
 * @author Cristian TG
 * @author Daniel RS
 * @author Borja CR
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 *
 */
package uva.compubi.domopi.webiopi;