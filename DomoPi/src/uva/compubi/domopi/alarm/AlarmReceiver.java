package uva.compubi.domopi.alarm;

import java.util.Hashtable;

import uva.compubi.domopi.R;
import uva.compubi.domopi.activity.CommonFunctions;
import uva.compubi.domopi.activity.MainActivity;
import uva.compubi.domopi.webiopi.WebIOPiClient;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.os.AsyncTask;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;

/**
 * Comprobaciones de activaci�n de la alarma
 * 
 * Siempre y cuando cumpla las fechas m�nima y m�xima de la notificaci�n y tenga
 * un ID mayor al �ltimo mostrado:
 * 
 * 
 * Vibra y suena la notificaci�n si el dispositivo lo tiene habilitado para que
 * el usuario no experimente malas sensaciones.
 * 
 * @author Cristian TG
 * @author Daniel RS
 * @author Borja CR
 * @since 0.2-BETA
 * @version 0.2-BETA
 */
public final class AlarmReceiver extends BroadcastReceiver {

	/**
	 * Contexto actual.
	 */
	private Context mContext;

	/**
	 * Tipo de alarma actual.
	 */
	private byte mAlarmType;

	/**
	 * Mensaje para las notificaciones.
	 */
	private String text;

	@SuppressWarnings("unchecked")
	@Override
	public void onReceive(final Context context, final Intent intent) {
		this.mContext = context;
		if (intent != null) {

			final SharedPreferences sharedPreferences = CommonFunctions
					.getSharedPreferences(context);

			final String ACTION = intent.getAction();
			
			// A. Proviene de reinicio del dispositivo.
			if (ACTION != null
					&& ACTION.equals("android.intent.action.BOOT_COMPLETED")) {
				// Establecer alarmas ya que al menos hay una activada (checkbox
				// del usuario)

				try {

					if (sharedPreferences.getBoolean(
							CommonFunctions.KEY_ON_HOUR_MIN, false)) {
						
						final String alarm = sharedPreferences.getString(
								CommonFunctions.KEY_ON_HOUR_MIN_TIME, null);
						if (alarm != null) {

							final String[] time = alarm
									.split(CommonFunctions.TIME_SEPARATOR);
							AlarmController.createAlarm(context,
									Integer.parseInt(time[0]),
									Integer.parseInt(time[1]),
									AlarmController.ALARM_MODE_ON);
						}
					}

					if (sharedPreferences.getBoolean(
							CommonFunctions.KEY_OFF_HOUR_MIN, false)) {
						final String alarm = sharedPreferences.getString(
								CommonFunctions.KEY_OFF_HOUR_MIN_TIME, null);
						if (alarm != null) {
							final String[] time = alarm
									.split(CommonFunctions.TIME_SEPARATOR);
							AlarmController.createAlarm(context,
									Integer.parseInt(time[0]),
									Integer.parseInt(time[1]),
									AlarmController.ALARM_MODE_OFF);
						}
					}
				} catch (final Exception ex) {

				}

			}

			// B. Proviene de aviso de alarma
			final byte alarmType = intent.getByteExtra(
					AlarmController.KEY_ALARM_MODE,
					AlarmController.ALARM_MODE_NOT_SET);
			if (alarmType == AlarmController.ALARM_MODE_ON
					|| alarmType == AlarmController.ALARM_MODE_OFF) {

				// Buscar qu� elementos est�n activados (checkbox)
				// No distingue entre aquellos de ON y OFF, en el futuro si se
				// quiere discriminar habr�a que cambiar esta parte.
				final int CHANNEL_LENGTH_1 = CommonFunctions.CHANNELS.length - 1;
				int currentChannel;
				final Hashtable<Integer, Boolean> mHashtable = new Hashtable<Integer, Boolean>();
				final boolean action = alarmType == AlarmController.ALARM_MODE_ON;
				final String TEXT_SEPARATOR = ", ";
				// Importante reinicializarlo.
				this.text = "";
				for (int i = CHANNEL_LENGTH_1; i >= 0; i--) {
					currentChannel = CommonFunctions.CHANNELS[i];
					if (sharedPreferences
							.getBoolean("" + currentChannel, false)) {
						this.text = this.text + currentChannel + TEXT_SEPARATOR;
						mHashtable.put(currentChannel, action);
					}

				}
				if (this.text.length() > 0) {
					this.text = this.text.substring(0,
							this.text.lastIndexOf(TEXT_SEPARATOR));
				}
				this.mAlarmType = alarmType;
				new SetGPIOStatusTask().execute(mHashtable);
			}
		}

	}

	/**
	 * Establece el valor (OUT) de los gpio's indicados.
	 * 
	 * @author Cristian TG
	 * @author Daniel RS
	 * @author Borja CR
	 * @since 0.2-BETA
	 * @version 0.2-BETA
	 */
	private final class SetGPIOStatusTask extends
			AsyncTask<Hashtable<Integer, Boolean>, Boolean, Boolean> {

		@Override
		protected Boolean doInBackground(
				final Hashtable<Integer, Boolean>... channelStatus) {
			try {
				final SharedPreferences sharedPreferences = CommonFunctions
						.getSharedPreferences(AlarmReceiver.this.mContext);
				new WebIOPiClient(sharedPreferences.getString(
						MainActivity.KEY_URL_SETTING, ""),
						sharedPreferences.getInt(MainActivity.KEY_PORT_SETTING,
								80), sharedPreferences.getString(
								MainActivity.KEY_USER_SETTING, ""),
						sharedPreferences.getString(
								MainActivity.KEY_PASSWORD_SETTING, ""))
						.setGPIOStatus(channelStatus[0]);
				return false;
			} catch (final Exception ex) {
				return true;
			}

		}

		@Override
		protected void onPostExecute(final Boolean showErrorMessage) {

			// 0. Obtener el estado actual del sonido en el dispositivo
			final AudioManager am = (AudioManager) AlarmReceiver.this.mContext
					.getSystemService(Context.AUDIO_SERVICE);
			final int ringerMode = am == null ? AudioManager.RINGER_MODE_SILENT
					: am.getRingerMode();

			// 1.Definir la vibracion del telefono si tiene vibraci�n activa.
			if (ringerMode == AudioManager.RINGER_MODE_VIBRATE
					|| ringerMode == AudioManager.RINGER_MODE_NORMAL) {
				try {
					((Vibrator) AlarmReceiver.this.mContext
							.getSystemService(Context.VIBRATOR_SERVICE))
							.vibrate(new long[] { 0, 250, 200, 250, 0 }, -1);
				} catch (final Exception e) {
				}
			}

			// 2. Crear la notificaci�n
			final NotificationCompat.Builder builder = new NotificationCompat.Builder(
					AlarmReceiver.this.mContext);

			final String title = AlarmReceiver.this.mAlarmType == AlarmController.ALARM_MODE_ON ? AlarmReceiver.this.mContext
					.getString(R.string.layout_normal_mode_auto_on_title)
					: AlarmReceiver.this.mContext
							.getString(R.string.layout_normal_mode_auto_off_title);

			final String messageText = showErrorMessage ? AlarmReceiver.this.mContext
					.getString(R.string.layout_normal_mode_error)
					: AlarmReceiver.this.mContext
							.getString(R.string.layout_normal_mode_gpio_selected)
							+ ' ' + AlarmReceiver.this.text;

			builder.setSmallIcon(android.R.drawable.ic_dialog_info)
					.setLargeIcon(
							BitmapFactory.decodeResource(
									AlarmReceiver.this.mContext.getResources(),
									R.drawable.ic_launcher))
					.setTicker(title + ' ' + messageText)
					.setWhen(System.currentTimeMillis())
					.setAutoCancel(true)
					.setContentTitle(title)
					.setContentText(messageText)
					.setStyle(
							new NotificationCompat.BigTextStyle()
									.bigText(messageText));
			final Notification noti = builder.build();

			// 3. Sonido
			if (ringerMode == AudioManager.RINGER_MODE_NORMAL) {
				try {
					noti.sound = RingtoneManager
							.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
				} catch (final Exception e) {
				}
			}

			// 4. Parpadeo
			// durante 400 ms y luego este apagado durante 1 segundo. Esta
			// secuencia
			// se repetira de
			// forma ciclica hasta que el usuario atienda la notificacion.
			try {
				noti.flags |= Notification.FLAG_SHOW_LIGHTS;
				try {
					noti.ledARGB = 0xFF00FFFF; // morado de pucela
				} catch (final Exception e) {
					noti.ledARGB = 0xFF0000FF; // azul, que es general
				}
				noti.ledOnMS = 400;
				noti.ledOffMS = 1000;
			} catch (final Exception e) {
			}

			// 5. Mostrar la notificaci�n
			try {
				((NotificationManager) AlarmReceiver.this.mContext
						.getSystemService(Context.NOTIFICATION_SERVICE))
						.notify(AlarmReceiver.this.mAlarmType, noti);
			} catch (final Exception ex) {
			}
		}
	}
}