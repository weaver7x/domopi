package uva.compubi.domopi.alarm;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

/**
 * Gestiona la creaci�n y desactivaci�n de alarmas.
 * 
 * @author Cristian TG
 * @author Daniel RS
 * @author Borja CR
 * @since 0.2-BETA
 * @version 0.2-BETA
 */
public final class AlarmController {

	/**
	 * Clave para indicar el tipo de alarma.
	 */
	final static String KEY_ALARM_MODE = "KEY_ALARM_MODE";

	/**
	 * Bandera que indica que la alarma no est� establecida.
	 */
	public final static byte ALARM_MODE_NOT_SET = -1;

	/**
	 * Bandera que indica que la alarma es de encendido (ON).
	 */
	public final static byte ALARM_MODE_ON = 0;

	/**
	 * Bandera que indica que la alarma es de apagado (OFF).
	 */
	public final static byte ALARM_MODE_OFF = 1;

	/**
	 * Identificador del aviso pendiente modo ON.
	 */
	private final static byte PENDING_INTENT_MODE_ON_ID = 2;

	/**
	 * Identificador del aviso pendiente modo OFF.
	 */
	private final static byte PENDING_INTENT_MODE_OFF_ID = 3;

	/**
	 * Crea una alarma de aviso de encendido/apagado autom�tico. Habilita el
	 * BroadcastReceiver {@link AlarmReceiver} para recibir eventos de reinicio
	 * del dispositivo y as� crear y recibir las alarmas correspondientes.
	 * 
	 * @param context
	 *            Contexto de uso
	 * @param hourOfDay
	 *            Hora del d�a a establecer la alarma.
	 * @param minutes
	 *            Minutos de la hora del d�a a establecer la alarma.
	 * @param alarmType
	 *            Clave del tipo de alarma. Ver {@link #ALARM_MODE_ON} y
	 *            {@link #ALARM_MODE_OFF}.
	 * @return True si se pudo crear la alarma. False en otro caso
	 */
	public static boolean createAlarm(final Context context,
			final int hourOfDay, final int minutes, final byte alarmType) {
		try {
			// 1. Activar el broadcast-receiver
			context.getPackageManager().setComponentEnabledSetting(
					new ComponentName(context, AlarmReceiver.class),
					PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
					PackageManager.DONT_KILL_APP);

			// 2. Establecer la alarma
			final Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(System.currentTimeMillis());
			calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
			calendar.set(Calendar.MINUTE, minutes);
			calendar.set(Calendar.SECOND, 0);

			// Evitar que lance la alarma en el momento si la fecha indicada
			// para la alarma es anterior a la fecha actual del dispositivo
			if ((Calendar.getInstance().getTimeInMillis() - calendar
					.getTimeInMillis()) > 0) {
				calendar.add(Calendar.DATE, 1);
			}

			((AlarmManager) context.getSystemService(Context.ALARM_SERVICE))
					.setRepeating(AlarmManager.RTC, calendar.getTimeInMillis(),
							AlarmManager.INTERVAL_DAY,
							createPendingIntent(context, alarmType));

			return true;
		} catch (final Exception ex) {
			// No se pudo acceder al servicio de alarmas.
			return false;
		}
	}

	/**
	 * Crea un aviso pendiente.
	 * 
	 * @param context
	 *            Contexto de uso.
	 * @param alarmType
	 *            Clave del tipo de alarma. Ver {@link #ALARM_MODE_ON} y
	 *            {@link #ALARM_MODE_OFF}.
	 * @return Aviso pendiente seg�n el modo de alarma ON u OFF.
	 */
	private static PendingIntent createPendingIntent(final Context context,
			final byte alarmType) {
		return PendingIntent.getBroadcast(context,
				alarmType == ALARM_MODE_ON ? PENDING_INTENT_MODE_ON_ID
						: PENDING_INTENT_MODE_OFF_ID, new Intent(context,
						AlarmReceiver.class)
						.putExtra(KEY_ALARM_MODE, alarmType),
				PendingIntent.FLAG_UPDATE_CURRENT);

	}

	/**
	 * Deshabilita el BroadcastReceiver {@link AlarmReceiver} para recibir
	 * eventos de reinicio del dispositivo y as� no recibir� ninguna alarma.
	 * 
	 * No deshabilita ning�n aviso pendiente. Para ello utilizar:
	 * {@link AlarmController#cancelPendingIntent}
	 * 
	 * @param context
	 *            Contexto de uso.
	 */
	public static void deleteAlarm(final Context context) {
		try {
			// Deshabilitar el broadcast-receiver
			context.getPackageManager().setComponentEnabledSetting(
					new ComponentName(context, AlarmReceiver.class),
					PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
					PackageManager.DONT_KILL_APP);
		} catch (final Exception ex) {
			// No se pudo acceder al servicio de alarmas.
		}
	}

	/**
	 * Cancela el aviso pendiente del tipo de alarma especificado.
	 * 
	 * @param context
	 *            Contexto de uso.
	 * @param alarmType
	 *            Clave del tipo de alarma. Ver {@link #ALARM_MODE_ON} y
	 *            {@link #ALARM_MODE_OFF}.
	 */
	public static void cancelPendingIntent(final Context context,
			final byte alarmType) {
		try {
			final AlarmManager manager = (AlarmManager) context
					.getSystemService(Context.ALARM_SERVICE);
			if (manager != null) {
				manager.cancel(createPendingIntent(context, alarmType));
			}
		} catch (final Exception ex) {
		}
	}

}
