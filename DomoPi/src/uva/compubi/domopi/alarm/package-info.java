/**
 * Contiene todo lo necesario para gestionar alarmas.
 * 
 * @author Cristian TG
 * @author Daniel RS
 * @author Borja CR
 * @since 0.2-BETA
 * @version 0.2-BETA
 */
package uva.compubi.domopi.alarm;