package uva.compubi.domopi.activity;

import uva.compubi.domopi.R;
import uva.compubi.domopi.model.ConnectionSettings;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Actividad inicial de la aplicaci�n.
 * 
 * @author Cristian TG
 * @author Daniel RS
 * @author Borja CR
 * @since 0.1-ALPHA
 * @version 0.2-BETA
 */
public final class MainActivity extends Activity implements OnClickListener,
		OnCheckedChangeListener {

	/**
	 * Clave para almacenar las preferencias de: URL.
	 */
	public static String KEY_URL_SETTING = "UrlSetting";
	/**
	 * Clave para almacenar las preferencias de: Puerto.
	 */
	public static String KEY_PORT_SETTING = "PortSetting";
	/**
	 * Clave para almacenar las preferencias de: Usuario.
	 */
	public static String KEY_USER_SETTING = "UserSetting";
	/**
	 * Clave para almacenar las preferencias de: Contrase�a.
	 */
	public static String KEY_PASSWORD_SETTING = "PasswordSetting";
	/**
	 * Clave para indicar si almacenar o no las preferencias.
	 */
	private static String KEY_REMEMBER_DATA_SETTING = "RememberDataSetting";

	@Override
	public void onCreate(final Bundle saved) {
		// Establecer el layout
		super.onCreate(saved);
		setContentView(R.layout.layout_main);

		// Asignar escuchadores
		findViewById(R.id.layout_main_login_button_id).setOnClickListener(this);
		((CheckBox) findViewById(R.id.layout_main_remember_data_checkbox_id))
				.setOnCheckedChangeListener(this);
	}

	@Override
	public void onResume() {
		super.onResume();

		// Valores recordados en el dispositivo
		final SharedPreferences sharedPreferences = CommonFunctions
				.getSharedPreferences(this);
		final boolean isChecked = sharedPreferences.getBoolean(
				KEY_REMEMBER_DATA_SETTING, true);
		final CheckBox rememberDataCB = (CheckBox) findViewById(R.id.layout_main_remember_data_checkbox_id);
		rememberDataCB.setChecked(isChecked);

		if (isChecked) {
			((EditText) findViewById(R.id.layout_main_url_edit_id))
					.setText(sharedPreferences.getString(KEY_URL_SETTING, ""));
			final byte NO_PORT = -1;
			final int port = sharedPreferences
					.getInt(KEY_PORT_SETTING, NO_PORT);
			if (port > NO_PORT) {
				((EditText) findViewById(R.id.layout_main_port_edit_id))
						.setText("" + port);
			}
			((EditText) findViewById(R.id.layout_main_user_edit_id))
					.setText(sharedPreferences.getString(KEY_USER_SETTING, ""));
			((EditText) findViewById(R.id.layout_main_password_edit_id))
					.setText(sharedPreferences.getString(KEY_PASSWORD_SETTING,
							""));
		}
	}

	@Override
	public void onClick(final View v) {
		switch (v.getId()) {
		case R.id.layout_main_login_button_id:
			try {
				boolean isError = false;
				EditText currentEditText = (EditText) findViewById(R.id.layout_main_url_edit_id);

				final String url = currentEditText.getText().toString();
				if (url.length() == 0) {
					currentEditText.setError(this
							.getString(R.string.layout_main_empty_field));
					isError = true;
				}

				currentEditText = (EditText) findViewById(R.id.layout_main_port_edit_id);
				final String portString = currentEditText.getText().toString();
				int port = 0;
				if (portString.length() == 0) {
					currentEditText.setError(this
							.getString(R.string.layout_main_empty_field));
					isError = true;
				} else {
					port = Integer.parseInt(portString);
				}

				currentEditText = (EditText) findViewById(R.id.layout_main_user_edit_id);
				final String user = currentEditText.getText().toString();
				if (user.length() == 0) {
					currentEditText.setError(this
							.getString(R.string.layout_main_empty_field));
					isError = true;
				}

				currentEditText = (EditText) findViewById(R.id.layout_main_password_edit_id);
				final String password = currentEditText.getText().toString();
				if (password.length() == 0) {
					currentEditText.setError(this
							.getString(R.string.layout_main_empty_field));
					isError = true;
				}

				// Continuar si no hubo error
				if (!isError) {

					// Guardar preferencias
					if (((CheckBox) findViewById(R.id.layout_main_remember_data_checkbox_id))
							.isChecked()) {
						CommonFunctions.getSharedPreferences(this).edit()
								.putString(KEY_URL_SETTING, url)
								.putInt(KEY_PORT_SETTING, port)
								.putString(KEY_USER_SETTING, user)
								.putString(KEY_PASSWORD_SETTING, password)
								.commit();
					}

					// Logearse si hay conexi�n a la red
					if (CommonFunctions.isOnline(this)) {
						CommonFunctions.startActivityExtra(this,
								NormalModeActivity.class,
								NormalModeActivity.KEY_CONNECTION_SETTINGS,
								new ConnectionSettings(url, port, user,
										password));
					} else {
						Toast.makeText(this, R.string.layout_normal_mode_error,
								Toast.LENGTH_LONG).show();
					}
				}

			} catch (final Exception ex) {
				// Se mantiene todo como estaba.
			}
			break;
		}
	}

	@Override
	public void onCheckedChanged(final CompoundButton buttonView,
			final boolean isChecked) {
		if (buttonView.getId() == R.id.layout_main_remember_data_checkbox_id) {
			CommonFunctions.getSharedPreferences(this).edit()
					.putBoolean(KEY_REMEMBER_DATA_SETTING, isChecked).commit();
		}
	}
}