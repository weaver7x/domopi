package uva.compubi.domopi.activity;

import java.util.Hashtable;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import uva.compubi.domopi.R;
import uva.compubi.domopi.alarm.AlarmController;
import uva.compubi.domopi.design.SegmentedGroup;
import uva.compubi.domopi.model.ConnectionSettings;
import uva.compubi.domopi.webiopi.WebIOPiClient;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TimePicker;
import android.widget.Toast;

/**
 * Actividad inicial de la aplicaci�n.
 * 
 * @author Cristian TG
 * @author Daniel RS
 * @author Borja CR
 * @since 0.1-ALPHA
 * @version 0.2-BETA
 */
public final class NormalModeActivity extends FragmentActivity implements
		OnClickListener, OnCheckedChangeListener,
		android.widget.CompoundButton.OnCheckedChangeListener,
		OnTimeSetListener {

	@Override
	public void onCreate(final Bundle saved) {

		// Establecer el layout
		super.onCreate(saved);
		setContentView(R.layout.layout_normal_mode);

		// Obtener la configuraci�n de conexi�n con el servidor WebIOPi
		final ConnectionSettings mConnectionSettings = (ConnectionSettings) getIntent()
				.getSerializableExtra(KEY_CONNECTION_SETTINGS);
		this.mWebIOPiClient = new WebIOPiClient(mConnectionSettings.getUrl(),
				mConnectionSettings.getPort(), mConnectionSettings.getUser(),
				mConnectionSettings.getPassword());

		// Asignar escuchadores de eventos
		this.mProgressBar = (ProgressBar) findViewById(R.id.layout_normal_mode_progress_bar_id);
		findViewById(R.id.layout_normal_mode_web_mode_button_id)
				.setOnClickListener(this);
		findViewById(R.id.layout_normal_mode_exit_button_id)
				.setOnClickListener(this);
		findViewById(R.id.layout_normal_mode_auto_on_button_id)
				.setOnClickListener(this);
		findViewById(R.id.layout_normal_mode_auto_off_button_id)
				.setOnClickListener(this);
		final int CHANNELS_LENGTH_1 = CommonFunctions.CHANNELS.length - 1;
		// Segmentos de botones ON/OFF para los gpio
		final int[] segmentedGroups = new int[] {
				R.id.layout_normal_mode_gpio7_button_id,
				R.id.layout_normal_mode_gpio8_button_id,
				R.id.layout_normal_mode_gpio9_button_id,
				R.id.layout_normal_mode_gpio11_button_id,
				R.id.layout_normal_mode_gpio25_button_id,
				R.id.layout_normal_mode_gpio_s10_button_id };
		// Checkboxes para los gpio
		final int[] checkButtons = new int[] {
				R.id.layout_normal_mode_gpio7_check_id,
				R.id.layout_normal_mode_gpio8_check_id,
				R.id.layout_normal_mode_gpio9_check_id,
				R.id.layout_normal_mode_gpio11_check_id,
				R.id.layout_normal_mode_gpio25_check_id,
				R.id.layout_normal_mode_gpio_s10_check_id };
		for (int i = CHANNELS_LENGTH_1; i >= 0; i--) {
			((SegmentedGroup) findViewById(segmentedGroups[i]))
					.setOnCheckedChangeListener(this);
			((CheckBox) findViewById(checkButtons[i]))
					.setOnCheckedChangeListener(this);
		}
		((CheckBox) findViewById(R.id.layout_normal_mode_auto_check_on))
				.setOnCheckedChangeListener(this);
		((CheckBox) findViewById(R.id.layout_normal_mode_auto_check_off))
				.setOnCheckedChangeListener(this);
	}

	@Override
	public void onResume() {
		super.onResume();

		// Para evitar recibir eventos absurdos
		this.isSetup = true;

		// Habilitar los botones de ON/OFF de los gpio
		enableSegmentedGroups(true);

		// Deshabilitar la barra de carga
		this.mProgressBar.setVisibility(View.GONE);

		// Refresco cada 5 segundos
		if (this.timer != null) {
			this.timer.cancel();
		}
		this.timer = new Timer();
		this.timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				NormalModeActivity.this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						enableSegmentedGroups(false);
						NormalModeActivity.this.isRefreshsing = true;
						new CheckGPIOStatusTask().execute();

						NormalModeActivity.this.mProgressBar
								.setVisibility(View.VISIBLE);
					}
				});
			}
		}, 0, 5000);

		// Establecer habilitado/deshabilitado en aquellos gpio indicados por el
		// usuario en preferencias
		final int CHANNEL_LENGTH_1 = CommonFunctions.CHANNELS.length - 1;
		final SharedPreferences sharedPreferences = CommonFunctions
				.getSharedPreferences(this);
		int currentChannel;
		for (int i = CHANNEL_LENGTH_1; i >= 0; i--) {
			currentChannel = CommonFunctions.CHANNELS[i];
			((CheckBox) findViewById(currentChannel == CommonFunctions.CHANNEL_7 ? R.id.layout_normal_mode_gpio7_check_id
					: currentChannel == CommonFunctions.CHANNEL_8 ? R.id.layout_normal_mode_gpio8_check_id
							: currentChannel == CommonFunctions.CHANNEL_9 ? R.id.layout_normal_mode_gpio9_check_id
									: currentChannel == CommonFunctions.CHANNEL_11 ? R.id.layout_normal_mode_gpio11_check_id
											: currentChannel == CommonFunctions.CHANNEL_25 ? R.id.layout_normal_mode_gpio25_check_id
													: R.id.layout_normal_mode_gpio_s10_check_id))
					.setChecked(sharedPreferences.getBoolean(""
							+ currentChannel, false));
		}

		// Habilitado/Deshabilitado las alarmas de ON y OFF autom�ticas
		((CheckBox) findViewById(R.id.layout_normal_mode_auto_check_on))
				.setChecked(sharedPreferences.getBoolean(
						CommonFunctions.KEY_ON_HOUR_MIN, false));
		((CheckBox) findViewById(R.id.layout_normal_mode_auto_check_off))
				.setChecked(sharedPreferences.getBoolean(
						CommonFunctions.KEY_OFF_HOUR_MIN, false));

		// Horarios guardados en las alarmas autom�ticas
		((Button) findViewById(R.id.layout_normal_mode_auto_on_button_id))
				.setText(sharedPreferences.getString(
						CommonFunctions.KEY_ON_HOUR_MIN_TIME,
						this.getString(R.string.layout_normal_mode_auto_on_hint)));
		((Button) findViewById(R.id.layout_normal_mode_auto_off_button_id))
				.setText(sharedPreferences.getString(
						CommonFunctions.KEY_OFF_HOUR_MIN_TIME,
						this.getString(R.string.layout_normal_mode_auto_off_hint)));

		this.isSetup = false;
	}

	/**
	 * Clave para el intercambio de la configuraci�n de conexi�n con el servidor
	 * WebIOpi.
	 */
	static String KEY_CONNECTION_SETTINGS = "KeyConnectionSettings";

	/**
	 * Contador de refresco.
	 */
	private Timer timer;

	/**
	 * Cliente con el servidor WebIOPi.
	 */
	private WebIOPiClient mWebIOPiClient;

	/**
	 * Barra de carga de progreso.
	 */
	private ProgressBar mProgressBar;

	/**
	 * Bandera que indica si se est� estableciendo los datos en la pantalla.
	 */
	private boolean isSetup;

	/**
	 * Botones ON de los GPIO.
	 */
	final static int[] onButtons = new int[] {
			R.id.layout_normal_mode_gpio7_radio_ON,
			R.id.layout_normal_mode_gpio8_radio_ON,
			R.id.layout_normal_mode_gpio9_radio_ON,
			R.id.layout_normal_mode_gpio11_radio_ON,
			R.id.layout_normal_mode_gpio25_radio_ON,
			R.id.layout_normal_mode_gpio_s10_radio_ON, };

	/**
	 * Botones OFF de los GPIO.
	 */
	final static int[] offButtons = new int[] {
			R.id.layout_normal_mode_gpio7_radio_OFF,
			R.id.layout_normal_mode_gpio8_radio_OFF,
			R.id.layout_normal_mode_gpio9_radio_OFF,
			R.id.layout_normal_mode_gpio11_radio_OFF,
			R.id.layout_normal_mode_gpio25_radio_OFF,
			R.id.layout_normal_mode_gpio_s10_radio_OFF };

	/**
	 * Habilita o deshabilita los botones gpio.
	 * 
	 * @param enable
	 *            True si habilita los botones, False en caso contrario.
	 */
	private final void enableSegmentedGroups(boolean enable) {

		final int ON_BUTTONS_LENGTH_1 = onButtons.length - 1;
		for (int i = ON_BUTTONS_LENGTH_1; i >= 0; i--) {
			((RadioButton) findViewById(onButtons[i])).setEnabled(enable);
		}
		final int OFF_BUTTONS_LENGTH_1 = offButtons.length - 1;
		for (int i = OFF_BUTTONS_LENGTH_1; i >= 0; i--) {
			((RadioButton) findViewById(offButtons[i])).setEnabled(enable);
		}
	}

	@Override
	public void onPause() {
		// Para evitar que siga calculando en segundo plano.
		if (this.timer != null) {
			this.timer.cancel();
		}
		super.onPause();
	}

	@Override
	public void onDestroy() {
		// Para evitar que siga calculando en segundo plano.
		if (this.timer != null) {
			this.timer.cancel();
		}
		super.onDestroy();
	}

	/**
	 * Obtiene el valor (OUT) de los gpio's indicados.
	 * 
	 * @author Cristian TG
	 * @author Daniel RS
	 * @author Borja CR
	 * @since 0.1-ALPHA
	 * @version 0.1-ALPHA
	 */
	private final class CheckGPIOStatusTask extends
			AsyncTask<Void, Boolean, Hashtable<Integer, Boolean>> {

		@Override
		protected Hashtable<Integer, Boolean> doInBackground(
				final Void... params) {

			try {
				return NormalModeActivity.this.mWebIOPiClient
						.getGPIOStatus(CommonFunctions.CHANNELS);
			} catch (final Exception ex) {
				ex.printStackTrace();
				return null;
			}
		}

		@Override
		protected void onPostExecute(
				final Hashtable<Integer, Boolean> channelStatus) {
			NormalModeActivity.this.mProgressBar.setVisibility(View.GONE);
			if (channelStatus == null) {
				Toast.makeText(NormalModeActivity.this,
						R.string.layout_normal_mode_error, Toast.LENGTH_LONG)
						.show();
			} else {
				final Set<Integer> keys = channelStatus.keySet();
				for (final Integer key : keys) {
					switch (key) {
					case CommonFunctions.CHANNEL_7:
						((RadioGroup) NormalModeActivity.this
								.findViewById(R.id.layout_normal_mode_gpio7_button_id))
								.check((Boolean) channelStatus.get(key) ? R.id.layout_normal_mode_gpio7_radio_ON
										: R.id.layout_normal_mode_gpio7_radio_OFF);
						break;
					case CommonFunctions.CHANNEL_8:
						((RadioGroup) NormalModeActivity.this
								.findViewById(R.id.layout_normal_mode_gpio8_button_id))
								.check((Boolean) channelStatus.get(key) ? R.id.layout_normal_mode_gpio8_radio_ON
										: R.id.layout_normal_mode_gpio8_radio_OFF);
						break;
					case CommonFunctions.CHANNEL_9:
						((RadioGroup) NormalModeActivity.this
								.findViewById(R.id.layout_normal_mode_gpio9_button_id))
								.check((Boolean) channelStatus.get(key) ? R.id.layout_normal_mode_gpio9_radio_ON
										: R.id.layout_normal_mode_gpio9_radio_OFF);
						break;
					case CommonFunctions.CHANNEL_11:
						((RadioGroup) NormalModeActivity.this
								.findViewById(R.id.layout_normal_mode_gpio11_button_id))
								.check((Boolean) channelStatus.get(key) ? R.id.layout_normal_mode_gpio11_radio_ON
										: R.id.layout_normal_mode_gpio11_radio_OFF);
						break;
					case CommonFunctions.CHANNEL_25:
						((RadioGroup) NormalModeActivity.this
								.findViewById(R.id.layout_normal_mode_gpio25_button_id))
								.check((Boolean) channelStatus.get(key) ? R.id.layout_normal_mode_gpio25_radio_ON
										: R.id.layout_normal_mode_gpio25_radio_OFF);
						break;
					case CommonFunctions.CHANNEL_10:
						((RadioGroup) NormalModeActivity.this
								.findViewById(R.id.layout_normal_mode_gpio_s10_button_id))
								.check((Boolean) channelStatus.get(key) ? R.id.layout_normal_mode_gpio_s10_radio_ON
										: R.id.layout_normal_mode_gpio_s10_radio_OFF);
						break;
					}

				}
			}
			enableSegmentedGroups(true);
			NormalModeActivity.this.isRefreshsing = false;
		}
	}

	/**
	 * Bandera que indica si se est� actualizando el valor de los elementos a
	 * trav�s de los datos obtenidos en la red.
	 */
	private boolean isRefreshsing;

	/**
	 * Establece el valor (OUT) de los gpio's indicados.
	 * 
	 * @author Cristian TG
	 * @author Daniel RS
	 * @author Borja CR
	 * @since 0.1-ALPHA
	 * @version 0.1-ALPHA
	 */
	private final class SetGPIOStatusTask extends
			AsyncTask<Hashtable<Integer, Boolean>, Boolean, Boolean> {

		@Override
		protected Boolean doInBackground(
				final Hashtable<Integer, Boolean>... channelStatus) {
			try {
				NormalModeActivity.this.mWebIOPiClient
						.setGPIOStatus(channelStatus[0]);
				return false;
			} catch (final Exception ex) {
				return true;
			}

		}

		@Override
		protected void onPostExecute(final Boolean showErrorMessage) {
			enableSegmentedGroups(true);
			if (showErrorMessage) {
				Toast.makeText(NormalModeActivity.this,
						R.string.layout_normal_mode_error, Toast.LENGTH_LONG)
						.show();
			}
		}
	}

	/**
	 * Bandera para indicar el modo de selecci�n de hora.
	 */
	boolean onMode;

	@Override
	public void onClick(final View v) {
		switch (v.getId()) {
		case R.id.layout_normal_mode_web_mode_button_id:
			CommonFunctions.startActivityExtra(this, WebModeActivity.class,
					KEY_CONNECTION_SETTINGS, (ConnectionSettings) getIntent()
							.getSerializableExtra(KEY_CONNECTION_SETTINGS));
			break;

		case R.id.layout_normal_mode_exit_button_id:
			CommonFunctions.startActivityDeletePrevious(this,
					MainActivity.class);
			break;

		case R.id.layout_normal_mode_auto_on_button_id:
			this.onMode = true;
			new TimePickerFragment(this).show(getSupportFragmentManager(),
					"on_picker");
			break;

		case R.id.layout_normal_mode_auto_off_button_id:
			this.onMode = false;
			new TimePickerFragment(this).show(getSupportFragmentManager(),
					"off_picker");
			break;
		}
	}

	@Override
	public void onTimeSet(final TimePicker view, final int hourOfDay,
			final int minute) {
		final String timeText = ""
				+ (hourOfDay < 10 ? ("0" + hourOfDay) : hourOfDay)
				+ CommonFunctions.TIME_SEPARATOR
				+ (minute < 10 ? ("0" + minute) : minute);

		// 1.Guardar el preferencias
		final SharedPreferences sharedPreferences = CommonFunctions
				.getSharedPreferences(this);
		sharedPreferences
				.edit()
				.putString(
						this.onMode ? CommonFunctions.KEY_ON_HOUR_MIN_TIME
								: CommonFunctions.KEY_OFF_HOUR_MIN_TIME,
						timeText).commit();

		// 2. Guardar en el bot�n
		((Button) findViewById(this.onMode ? R.id.layout_normal_mode_auto_on_button_id
				: R.id.layout_normal_mode_auto_off_button_id))
				.setText(timeText);

		// 3. Habilitar nueva alarma si su checkbox est� habilitado
		final byte alarmType = this.onMode ? AlarmController.ALARM_MODE_ON
				: AlarmController.ALARM_MODE_OFF;
		if (sharedPreferences.getBoolean(
				this.onMode ? CommonFunctions.KEY_ON_HOUR_MIN
						: CommonFunctions.KEY_OFF_HOUR_MIN, false)) {
			// 1. Desactivar alarma antigua
			AlarmController.cancelPendingIntent(this, alarmType);
			// 2. Establecer nueva alarma
			AlarmController.createAlarm(this, hourOfDay, minute, alarmType);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onCheckedChanged(final RadioGroup group, final int checkedId) {

		final byte NO_CHANNEL = -1;
		int channel = NO_CHANNEL;
		switch (group.getId()) {
		case R.id.layout_normal_mode_gpio7_button_id:
			channel = CommonFunctions.CHANNEL_7;
			break;
		case R.id.layout_normal_mode_gpio8_button_id:
			channel = CommonFunctions.CHANNEL_8;
			break;
		case R.id.layout_normal_mode_gpio9_button_id:
			channel = CommonFunctions.CHANNEL_9;
			break;
		case R.id.layout_normal_mode_gpio11_button_id:
			channel = CommonFunctions.CHANNEL_11;
			break;
		case R.id.layout_normal_mode_gpio25_button_id:
			channel = CommonFunctions.CHANNEL_25;
			break;
		case R.id.layout_normal_mode_gpio_s10_button_id:
			channel = CommonFunctions.CHANNEL_10;
			break;
		}
		// Importante no volver a llamar en caso de que sea un cambio
		// proveniente del servidor.
		if (!this.isRefreshsing && channel != NO_CHANNEL) {
			try {
				final Hashtable<Integer, Boolean> mHashtable = new Hashtable<Integer, Boolean>();
				mHashtable
						.put(channel,
								checkedId == R.id.layout_normal_mode_gpio7_radio_ON
										|| checkedId == R.id.layout_normal_mode_gpio8_radio_ON
										|| checkedId == R.id.layout_normal_mode_gpio9_radio_ON
										|| checkedId == R.id.layout_normal_mode_gpio11_radio_ON
										|| checkedId == R.id.layout_normal_mode_gpio25_radio_ON
										|| checkedId == R.id.layout_normal_mode_gpio_s10_radio_ON);
				enableSegmentedGroups(false);
				new SetGPIOStatusTask().execute(mHashtable);
			} catch (final Exception ex) {
			}
		}

	}

	@Override
	public void onCheckedChanged(final CompoundButton buttonView,
			final boolean isChecked) {
		// Recibir evento si no se est� mostrando la interfaz por primera vez
		if (!this.isSetup) {
			// 1. Guardar el cambio en preferencias
			final int id = buttonView.getId();
			final SharedPreferences sharedPreferences = CommonFunctions
					.getSharedPreferences(this);
			sharedPreferences
					.edit()
					.putBoolean(
							""
									+ ((id == R.id.layout_normal_mode_gpio7_check_id) ? CommonFunctions.CHANNEL_7
											: (id == R.id.layout_normal_mode_gpio8_check_id) ? CommonFunctions.CHANNEL_8
													: (id == R.id.layout_normal_mode_gpio9_check_id) ? CommonFunctions.CHANNEL_9
															: (id == R.id.layout_normal_mode_gpio11_check_id) ? CommonFunctions.CHANNEL_11
																	: (id == R.id.layout_normal_mode_gpio25_check_id) ? CommonFunctions.CHANNEL_25
																			: (id == R.id.layout_normal_mode_gpio_s10_check_id) ? CommonFunctions.CHANNEL_10
																					: (id == R.id.layout_normal_mode_auto_check_on) ? CommonFunctions.KEY_ON_HOUR_MIN
																							: CommonFunctions.KEY_OFF_HOUR_MIN

									), isChecked).commit();

			// 2. Comprobar si hay que desactivar o activar las alarmas
			if (id == R.id.layout_normal_mode_auto_check_on
					|| id == R.id.layout_normal_mode_auto_check_off) {
				final boolean isOnMode = id == R.id.layout_normal_mode_auto_check_on;

				// 2.A. Activar alarma
				if (isChecked) {
					// 2.A.1. Obtener la alarma de preferencias
					final String alarm = sharedPreferences.getString(
							isOnMode ? CommonFunctions.KEY_ON_HOUR_MIN_TIME
									: CommonFunctions.KEY_OFF_HOUR_MIN_TIME,
							null);

					// 2.A.2. Crear la alarma
					if (alarm != null) {
						final String[] time = alarm
								.split(CommonFunctions.TIME_SEPARATOR);
						AlarmController.createAlarm(this, Integer
								.parseInt(time[0]), Integer.parseInt(time[1]),
								isOnMode ? AlarmController.ALARM_MODE_ON
										: AlarmController.ALARM_MODE_OFF);
					}

					// 2.B. Desactivar alarma
				} else {
					// 2.B.1. Desactivar alarma
					AlarmController.cancelPendingIntent(this,
							isOnMode ? AlarmController.ALARM_MODE_ON
									: AlarmController.ALARM_MODE_OFF);

					// 2.B.2. Desactivar en broadcast si no hay ning�n tipo de
					// alarma activo

					if (!(sharedPreferences.getBoolean(
							CommonFunctions.KEY_ON_HOUR_MIN, false) || sharedPreferences
							.getBoolean(CommonFunctions.KEY_OFF_HOUR_MIN, false))) {
						AlarmController.deleteAlarm(this);
					}
				}
			}
		}
	}
}