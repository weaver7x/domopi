package uva.compubi.domopi.activity;

import java.io.Serializable;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;

/**
 * Funciones comunes a las actividades.
 * 
 * @author Cristian TG
 * @author Daniel RS
 * @author Borja CR
 * @since 0.1-ALPHA
 * @version 0.2-BETA
 */
public final class CommonFunctions {

	/**
	 * Modo de lanzar actividades, manteniendo en la pila la actividad
	 * lanzadora.
	 */
	private static final byte START_ACTIVITY_NO_DELETE = 0;
	/**
	 * Modo de lanzar actividades, no manteniendo en la pila la actividad
	 * lanzadora.
	 */
	private static final byte START_ACTIVITY_DELETE_PREVIOUS = 1;

	/**
	 * Lanza una actividad eliminando de la pila de actividades la lanzadora.
	 * 
	 * @param fromActivity
	 *            Actividad lanzadora.
	 * @param toActivityClass
	 *            Actividad lanzada.
	 */
	static void startActivityDeletePrevious(final Activity fromActivity,
			final Class<?> toActivityClass) {
		CommonFunctions
				.newActivityDeletePrevious(fromActivity, toActivityClass);
	}

	/**
	 * Lanza una actividad manteniendo en la pila de actividades la lanzadora.
	 * 
	 * @param fromActivity
	 *            Actividad lanzadora.
	 * @param toActivityClass
	 *            Actividad lanzada.
	 */
	static void startActivity(final Activity fromActivity,
			final Class<?> toActivityClass) {
		CommonFunctions.newActivity(fromActivity, toActivityClass);
	}

	/**
	 * Lanza una actividad manteniendo en la pila de actividades la lanzadora
	 * con un objeto asociado.
	 * 
	 * @param fromActivity
	 *            Actividad lanzadora.
	 * @param toClass
	 *            Actividad lanzada.
	 * @param key
	 *            Clave del objeto pasado.
	 * @param value
	 *            Objeto pasado.
	 */
	static <T> void startActivityExtra(final Activity fromActivity,
			final Class<?> toClass, final String key, final T value) {
		CommonFunctions.newActivityExtra(fromActivity, toClass, key, value);
	}

	/**
	 * Lanza una actividad, no manteniendo en la pila la actividad lanzadora.
	 * 
	 * @param fromActivity
	 *            Actividad lanzadora.
	 * @param toClass
	 *            Actividad lanzada.
	 * @param key
	 *            Clave del objeto pasado.
	 * @param value
	 *            Objeto pasado.
	 */
	static <T> void startActivityExtraDeletePrevious(
			final Activity fromActivity, final Class<?> toClass,
			final String key, final T value) {
		CommonFunctions.newActivityExtraDeletePrevious(fromActivity, toClass,
				key, value);
	}

	/**
	 * Lanza una actividad eliminando de la pila de actividades la lanzadora.
	 * 
	 * @param fromActivity
	 *            Actividad lanzadora.
	 * @param toActivityClass
	 *            Actividad lanzada.
	 */
	private static void newActivityDeletePrevious(final Activity fromActivity,
			final Class<?> toActivityClass) {
		CommonFunctions.executeActivity(fromActivity, new Intent(fromActivity,
				toActivityClass), START_ACTIVITY_DELETE_PREVIOUS);
	}

	/**
	 * Lanza una actividad manteniendo en la pila de actividades la lanzadora.
	 * 
	 * @param fromActivity
	 *            Actividad lanzadora.
	 * @param toActivityClass
	 *            Actividad lanzada.
	 */
	private static void newActivity(final Activity fromActivity,
			final Class<?> toActivityClass) {
		CommonFunctions.executeActivity(fromActivity, new Intent(fromActivity,
				toActivityClass), START_ACTIVITY_NO_DELETE);
	}

	/**
	 * Lanza una actividad eliminando de la pila de actividades la lanzadora.
	 * 
	 * @param fromActivity
	 *            Actividad lanzadora.
	 * @param toClass
	 *            Actividad lanzada.
	 * @param key
	 *            Clave del objeto pasado.
	 * @param value
	 *            Objeto pasado.
	 */
	private static <T> void newActivityExtraDeletePrevious(
			final Activity fromActivity, final Class<?> toClass,
			final String key, final T value) {
		CommonFunctions.executeActivityExtra(fromActivity, toClass, key, value,
				START_ACTIVITY_DELETE_PREVIOUS);
	}

	/**
	 * Lanza una actividad manteniendo en la pila de actividades la lanzadora.
	 * con un objeto asociado serializable.
	 * 
	 * @param fromActivity
	 *            Actividad lanzadora.
	 * @param toClass
	 *            Actividad lanzada.
	 * @param key
	 *            Clave del objeto pasado.
	 * @param value
	 *            Objeto pasado.
	 */
	private static <T> void newActivityExtra(final Activity fromActivity,
			final Class<?> toClass, final String key, final T value) {
		CommonFunctions.executeActivityExtra(fromActivity, toClass, key, value,
				START_ACTIVITY_NO_DELETE);
	}

	/**
	 * Lanza una actividad segun el modo especificado. Si ocurre algun error se
	 * muestra un mensaje de error por UI.
	 * 
	 * @param activity
	 *            Actividad del contexto lanzadora.
	 * @param intent
	 *            Intencion que contiene el destino del lanzamiento.
	 * @param mode
	 *            Indica si se mantiene en la pila de anteriores actividades o
	 *            no.
	 */
	private static void executeActivity(final Activity activity,
			final Intent intent, final byte mode) {
		try {
			if (mode == START_ACTIVITY_DELETE_PREVIOUS) {
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				activity.finish();
			}
			activity.startActivity(intent);
		} catch (final Exception e) {
		}
	}

	/**
	 * Lanza una actividad segun el modo especificado con un objeto asociado
	 * serializable.
	 * 
	 * @param fromActivity
	 *            Actividad lanzadora.
	 * @param toClass
	 *            Actividad lanzada.
	 * @param key
	 *            Clave del objeto pasado.
	 * @param value
	 *            Objeto pasado.
	 * @param mode
	 *            Indica si se mantiene en la pila de anteriores actividades o
	 *            no.
	 */
	private static <T> void executeActivityExtra(final Activity fromActivity,
			final Class<?> toClass, final String key, final T value,
			final byte mode) {
		final Intent intent = new Intent(fromActivity, toClass);

		try {
			intent.putExtra(key, (Serializable) value);
			CommonFunctions.executeActivity(fromActivity, intent, mode);
		} catch (final Exception e) {
			try {
				intent.putExtra(key, "" + value);
				CommonFunctions.executeActivity(fromActivity, intent, mode);
			} catch (final Exception f) {

			}
		}
	}

	/**
	 * Comprueba si hay conexi�n (de cualquier tipo: WiFi y datos m�viles) a la
	 * red.
	 * 
	 * @param context
	 *            Contexto necesario de la aplicaci�n.
	 * @return True si hay conexi�n (Wifi o datos m�viles). False en caso
	 *         contrario.
	 */
	public static boolean isOnline(final Context context) {
		final ConnectivityManager service = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		final NetworkInfo netInfo = service != null ? service
				.getActiveNetworkInfo() : null;
		return (netInfo != null && netInfo.isConnected());
	}

	/**
	 * Obtiene las preferencias compartidas.
	 * 
	 * @param context
	 *            Contexto de uso.
	 * @return Preferencias compartidas.
	 */
	public static SharedPreferences getSharedPreferences(final Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context);
	}

	/**
	 * Clave para la preferencia (checkbox) de encendido autom�tico.
	 */
	public final static String KEY_ON_HOUR_MIN = "KEY_ON_H_M";

	/**
	 * Clave para la preferencia (checkbox) de apagado autom�tico.
	 */
	public final static String KEY_OFF_HOUR_MIN = "KEY_OFF_H_M";

	/**
	 * Clave para la preferencia (horario) de encendido autom�tico.
	 */
	public final static String KEY_ON_HOUR_MIN_TIME = "KEY_ON_H_M_TIME";

	/**
	 * Clave para la preferencia (horario) de apagado autom�tico.
	 */
	public final static String KEY_OFF_HOUR_MIN_TIME = "KEY_OFF_H_M_TIME";

	/**
	 * Separador de tiempo.
	 */
	public final static String TIME_SEPARATOR = ":";

	/**
	 * Canal 9 GPIO.
	 */
	public final static int CHANNEL_9 = 9;
	/**
	 * Canal 25 GPIO.
	 */
	public final static int CHANNEL_25 = 25;
	/**
	 * Canal 11 GPIO.
	 */
	public final static int CHANNEL_11 = 11;
	/**
	 * Canal 8 GPIO.
	 */
	public final static int CHANNEL_8 = 8;
	/**
	 * Canal 7 GPIO.
	 */
	public final static int CHANNEL_7 = 7;
	/**
	 * Canal 10 GPIO (Flexo).
	 */
	public final static int CHANNEL_10 = 10;
	/**
	 * Canales de los gpio.
	 */
	public final static int[] CHANNELS = new int[] { CHANNEL_9, CHANNEL_25,
			CHANNEL_11, CHANNEL_8, CHANNEL_7, CHANNEL_10 };
}