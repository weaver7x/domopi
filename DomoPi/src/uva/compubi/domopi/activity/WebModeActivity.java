package uva.compubi.domopi.activity;

import uva.compubi.domopi.R;
import uva.compubi.domopi.model.ConnectionSettings;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.HttpAuthHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Muestra en forma de {@link WebView} el contenido de la URL especificado.
 * 
 * @author Cristian TG
 * @author Daniel RS
 * @author Borja CR
 * @since 0.1-ALPHA
 * @version 1.0-INITIAL
 */
@SuppressLint("SetJavaScriptEnabled")
public final class WebModeActivity extends Activity implements OnClickListener {
	/**
	 * Vista de la web.
	 */
	protected WebView webView;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		// Establecer el layout
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_web_mode);

		// Asignar escuchadores
		findViewById(R.id.layout_web_mode_normal_mode_button_id)
				.setOnClickListener(this);
		findViewById(R.id.layout_web_mode_exit_button_id).setOnClickListener(
				this);

		// Aasignar botones
		this.webView = (WebView) findViewById(R.id.layout_web_mode_webview_id);

		if (WebModeActivity.this.webView != null) {
			final ConnectionSettings cs = (ConnectionSettings) getIntent()
					.getSerializableExtra(
							NormalModeActivity.KEY_CONNECTION_SETTINGS);
			this.webView.setWebViewClient(new MyWebViewClient(cs.getUser(), cs
					.getPassword()));
			final WebSettings settings = this.webView.getSettings();
			settings.setSupportZoom(false);
			settings.setJavaScriptEnabled(true);
			this.webView.loadUrl("http://157.88.110.145/");

		} else {
			this.finish();
		}
	}

	/**
	 * Cliente que permite autorizaci�n Auth Request v�a "prompt" .
	 * 
	 * @author Cristian TG
	 * @author Daniel RS
	 * @author Borja CR
	 * @since 0.1-ALPHA
	 * @version 0.1-ALPHA
	 */
	private class MyWebViewClient extends WebViewClient {
		String username = "";
		String password = "";

		private MyWebViewClient() {
		};

		private MyWebViewClient(final String username, final String password) {
			this.username = username;
			this.password = password;
		};

		@Override
		public void onReceivedHttpAuthRequest(final WebView view,
				final HttpAuthHandler handler, final String host,
				final String realm) {

			handler.proceed(this.username, this.password);

		}
	}

	@Override
	public boolean onKeyDown(final int keyCode, final KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_DOWN) {
			switch (keyCode) {
			case KeyEvent.KEYCODE_BACK:
				if (WebModeActivity.this.webView.canGoBack()) {
					WebModeActivity.this.webView.goBack();
				} else {
					WebModeActivity.this.freeCache();
					WebModeActivity.this.finish();
				}
				return true;
			}

		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * Libera la cach� del WebView.
	 */
	private void freeCache() {
		this.webView.clearHistory();
		this.webView.clearCache(true);
		this.webView.loadUrl("about:blank");
	}

	@Override
	public void onClick(final View v) {
		switch (v.getId()) {
		case R.id.layout_web_mode_normal_mode_button_id:
			CommonFunctions.startActivityExtra(
					this,
					NormalModeActivity.class,
					NormalModeActivity.KEY_CONNECTION_SETTINGS,
					(ConnectionSettings) getIntent().getSerializableExtra(
							NormalModeActivity.KEY_CONNECTION_SETTINGS));
			break;

		case R.id.layout_web_mode_exit_button_id:
			CommonFunctions.startActivityDeletePrevious(this,
					MainActivity.class);
			break;
		}
	}
}