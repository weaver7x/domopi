/**
 * Elementos de la interfaz gr�fica de la aplicaci�n.
 * 
 * @author Cristian TG
 * @author Daniel RS
 * @author Borja CR
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
package uva.compubi.domopi.design;