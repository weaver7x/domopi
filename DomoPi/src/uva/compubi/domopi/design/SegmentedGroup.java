package uva.compubi.domopi.design;

import uva.compubi.domopi.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

/**
 * 
 * Se encarga de los botones de "switch" con un dise�o especial.
 * 
 * @author Cristian TG
 * @author Daniel RS
 * @author Borja CR
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class SegmentedGroup extends RadioGroup {

	private int oneDP;
	private Resources resources;
	private int mTintColor;
	private int mCheckedTextColor = Color.WHITE;
	private Typeface mTypeface;

	/**
	 * Crea un bot�n con un contexto.
	 * 
	 * @param context
	 *            Contexto de uso.
	 */
	public SegmentedGroup(final Context context) {
		super(context);
		this.resources = getResources();
		this.mTypeface = null;
		this.mTintColor = this.resources
				.getColor(R.color.radio_button_selected_color);
		this.oneDP = (int) TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, 1,
				this.resources.getDisplayMetrics());
	}

	/**
	 * Crea un bot�n con un contexto y uns par�metros.
	 * 
	 * @param context
	 *            Contexto de uso.
	 * @param attrs
	 *            Artibutos a aplicar.
	 */
	public SegmentedGroup(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		this.resources = getResources();
		this.mTypeface = null;
		this.mTintColor = this.resources
				.getColor(R.color.radio_button_selected_color);
		this.oneDP = (int) TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, 1,
				this.resources.getDisplayMetrics());
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		// Use holo light for default
		updateBackground();
	}

	/**
	 * Establece el color de tinte.
	 * 
	 * @param tintColor
	 *            Color de tinte.
	 */
	public void setTintColor(final int tintColor) {
		this.mTintColor = tintColor;
		updateBackground();
	}

	/**
	 * Establece el color Tint.
	 * 
	 * @param tintColor
	 * @param checkedTextColor
	 */
	public void setTintColor(final int tintColor, final int checkedTextColor) {
		this.mTintColor = tintColor;
		this.mCheckedTextColor = checkedTextColor;
		updateBackground();
	}

	/**
	 * Actualiza el fondo de la vista.
	 */
	public void updateBackground() {
		int count = super.getChildCount();
		if (count > 1) {
			final View child = getChildAt(0);
			LayoutParams initParams = (LayoutParams) child.getLayoutParams();
			LayoutParams params = new LayoutParams(initParams.width,
					initParams.height, initParams.weight);
			// Check orientation for proper margins
			if (getOrientation() == LinearLayout.HORIZONTAL) {
				params.setMargins(0, 0, -this.oneDP, 0);
			} else {
				params.setMargins(0, 0, 0, -this.oneDP);
			}
			child.setLayoutParams(params);
			// Check orientation for proper layout
			if (getOrientation() == LinearLayout.HORIZONTAL) {
				updateBackground(getChildAt(0), R.drawable.radio_checked_left,
						R.drawable.radio_unchecked_left);
			} else {
				updateBackground(getChildAt(0), R.drawable.radio_checked_top,
						R.drawable.radio_unchecked_top);
			}
			View child2;
			for (int i = 1; i < count - 1; i++) {
				child2 = getChildAt(i);
				// Check orientation for proper layout
				if (getOrientation() == LinearLayout.HORIZONTAL) {
					updateBackground(child2, R.drawable.radio_checked_middle,
							R.drawable.radio_unchecked_middle);
				} else {
					// Middle radiobutton when checked is the same as
					// horizontal.
					updateBackground(child2, R.drawable.radio_checked_middle,
							R.drawable.radio_unchecked_middle_vertical);
				}
				// child2 = getChildAt(i);
				initParams = (LayoutParams) child2.getLayoutParams();
				params = new LayoutParams(initParams.width, initParams.height,
						initParams.weight);
				// Check orientation for proper margins
				if (getOrientation() == LinearLayout.HORIZONTAL) {
					params.setMargins(0, 0, -this.oneDP, 0);
				} else {
					params.setMargins(0, 0, 0, -this.oneDP);
				}
				child2.setLayoutParams(params);
			}
			// Check orientation for proper layout
			if (getOrientation() == LinearLayout.HORIZONTAL) {
				updateBackground(getChildAt(count - 1),
						R.drawable.radio_checked_right,
						R.drawable.radio_unchecked_right);
			} else {
				updateBackground(getChildAt(count - 1),
						R.drawable.radio_checked_bottom,
						R.drawable.radio_unchecked_bottom);
			}
		} else if (count == 1) {
			final View mView = getChildAt(0);
			updateBackground(mView, R.drawable.radio_checked_default,
					R.drawable.radio_unchecked_default);
		}
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	private void updateBackground(final View view, final int checked,
			final int unchecked) {
		final Button button = (Button) view;
		if (this.mTypeface != null) {
			button.setTypeface(this.mTypeface);
		}
		// Set text color
		button.setTextColor(new ColorStateList(
				new int[][] {
						{ android.R.attr.state_pressed },
						{ -android.R.attr.state_pressed,
								-android.R.attr.state_checked },
						{ -android.R.attr.state_pressed,
								android.R.attr.state_checked } }, new int[] {
						Color.GRAY, this.mTintColor, this.mCheckedTextColor }));

		// Redraw with tint color
		final Drawable checkedDrawable = this.resources.getDrawable(checked)
				.mutate();
		final Drawable uncheckedDrawable = this.resources
				.getDrawable(unchecked).mutate();
		((GradientDrawable) checkedDrawable).setColor(this.mTintColor);
		((GradientDrawable) uncheckedDrawable).setStroke(this.oneDP,
				this.mTintColor);

		// Create drawable
		final StateListDrawable stateListDrawable = new StateListDrawable();
		stateListDrawable.addState(new int[] { -android.R.attr.state_checked },
				uncheckedDrawable);
		stateListDrawable.addState(new int[] { android.R.attr.state_checked },
				checkedDrawable);

		// Para que funcione en versiones anteriores.
		// view.setBackgroundDrawable(stateListDrawable);
		// Set button background
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			view.setBackground(stateListDrawable);
		} else {
			view.setBackgroundDrawable(stateListDrawable);
		}

	}

	/**
	 * @return the mTypeface
	 */
	public Typeface getmTypeface() {
		return this.mTypeface;
	}

	/**
	 * Establece el valor de la tipograf�a. Debe llamarse desde el UI principal.
	 * 
	 * @param mTypeface
	 *            the mTypeface to set
	 */
	public void setmTypeface(final Typeface mTypeface) {
		this.mTypeface = mTypeface;
		updateBackground();
	}
}