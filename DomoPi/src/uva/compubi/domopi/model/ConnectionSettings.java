package uva.compubi.domopi.model;

import java.io.Serializable;

/**
 * Modela los datos necesarios para la conexi�n con un servidor WebIOPi.
 * 
 * @author Cristian TG
 * @author Daniel RS
 * @author Borja CR
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class ConnectionSettings implements Serializable {

	/**
	 * C�digo para la serializaci�n.
	 */
	private static final long serialVersionUID = 8194663756760920317L;
	/**
	 * URL de conexi�n con el servidor WebIOPi.
	 */
	private String url;
	/**
	 * Puerto de conexi�n con el servidor WebIOPi.
	 */
	private int port;
	/**
	 * Usuario de conexi�n con el servidor WebIOPi.
	 */
	private String user;
	/**
	 * Contrase�a de conexi�n con el servidor WebIOPi.
	 */
	private String password;

	/**
	 * Constructor por defecto. Inicializa todos los valores por defecto.
	 */
	public ConnectionSettings() {
		this.url = "";
		this.port = 80;
		this.user = "";
		this.password = "";
	}

	/**
	 * Constructor que especifica todos los posibles campos.
	 * 
	 * @param url
	 *            URL de conexi�n con el servidor WebIOPi.
	 * @param port
	 *            Puerto de conexi�n con el servidor WebIOPi.
	 * @param user
	 *            Usuario de conexi�n con el servidor WebIOPi.
	 * @param password
	 *            Contrase�a de conexi�n con el servidor WebIOPi.
	 */
	public ConnectionSettings(final String url, final int port,
			final String user, final String password) {
		super();
		this.url = url;
		this.port = port;
		this.user = user;
		this.password = password;
	}

	/**
	 * Obtiene la URL de conexi�n con el servidor WebIOPi.
	 * 
	 * @return URL de conexi�n con el servidor WebIOPi.
	 */
	public final String getUrl() {
		return this.url;
	}

	/**
	 * Establece la URL de conexi�n con el servidor WebIOPi.
	 * 
	 * @param url
	 *            URL de conexi�n con el servidor WebIOPi.
	 */
	public final void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Obtiene el puerto de conexi�n con el servidor WebIOPi.
	 * 
	 * @return Puerto de conexi�n con el servidor WebIOPi.
	 */
	public final int getPort() {
		return this.port;
	}

	/**
	 * Establece el puerto de conexi�n con el servidor WebIOPi.
	 * 
	 * @param port
	 *            the Puerto de conexi�n con el servidor WebIOPi.
	 */
	public final void setPort(int port) {
		this.port = port;
	}

	/**
	 * Obtiene el usuario de conexi�n con el servidor WebIOPi.
	 * 
	 * @return the user
	 */
	public final String getUser() {
		return this.user;
	}

	/**
	 * Establece el usuario de conexi�n con el servidor WebIOPi.
	 * 
	 * @param user
	 *            Usuario de conexi�n con el servidor WebIOPi.
	 */
	public final void setUser(String user) {
		this.user = user;
	}

	/**
	 * Obtiene la contrase�a de conexi�n con el servidor WebIOPi.
	 * 
	 * @return the password Contrase�a de conexi�n con el servidor WebIOPi.
	 */
	public final String getPassword() {
		return this.password;
	}

	/**
	 * Establece la contrase�a de conexi�n con el servidor WebIOPi.
	 * 
	 * @param password
	 *            Contrase�a de conexi�n con el servidor WebIOPi.
	 */
	public final void setPassword(String password) {
		this.password = password;
	}
}